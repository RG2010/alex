const express = require('express');

const mongoose = require('mongoose');

const bodyParser = require('body-parser');

const app = express();

const db = mongoose.connect('mongodb://localhost/alex');

const port = process.env.port || 4000;

const Gun = require('./models/gunModel');

const gunRouter = require('./routes/gunRouter')(Gun);

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());


app.use('/cod', gunRouter);
app.get('/', (req,res) => {
    res.send('Use /cod/guns In Your Browser Address Bar To See The Weapons List');
});
app.listen(port, () => {
    console.log(`Running On Port ${port} `);
});