const express = require('express');

const controller = require('../controller/gunController');

function routes(Gun) {
    const gunRouter = express.Router();

    const gunController = controller(Gun);

    gunRouter.route('/guns')
        .post(gunController.post)
        .get(gunController.get);
    gunRouter.use(`/guns/:gunsId`, (req, res, next) => {
        Gun.findById(req.params.gunId, (err, Gun) => {
            if (err) {
                return res.send(err);
            }
            if (Gun){
                req.Gun = Gun;
                return next();
            }
        });
    });
    gunRouter.route('/guns/:gunsId')
        .get((req, res) => res.json(req.gun))
        .put((req, res) => {
            const {gun} = req;
            gun.gunName = req.body.gunName;
            gun.gunType = req.body.gunType;
            gun.gunDamage = req.body.gunDamage;
            gun.gunFirerate = req.body.gunFirerate;
            gun.gunAccuracy = req.body.gunAccuracy;
            req.gun.save((err) => {
                if (err) {
                    return res.send(err);
                }
                return res.json(gun);
            });

        })
    .patch((req, res) => {
        const {gun} = req;
        if (req.body.id) {
            delete req.body.id;
        }
        Object.entries(req.body).forEach((item) => {
            const key = item[0];
            gun[key] = item[0];
        });
        req.gun.save((err) =>{
            if (err) {
                return res.send(err);
            }
            return res.json(gun);
         });
    });
    return gunRouter;

}

module.exports = routes;