const mongoose = require('mongoose');

const {Schema} = mongoose;

const gunModel = new Schema(
    {
        gunName: {type: String},
        gunType: {type: String},
        gunDamage: {type: Number},
        gunFirerate: {type: Number},
        gunAccuracy: {type: Number},
    });


module.exports = mongoose.model('Gun', gunModel);