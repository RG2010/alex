**Overview**

This API Is Based On Node.js It Will Give You The Weapon Details That Exists In Call Of Duty Mobile 

**Installation Guide**


**Before You Begin**

1.Make Sure That You Have Installed Node.js(15.14.0 Or Higher)
And Git


2.Follow The Below Instructions

3.Clone Repository

4.Change Your Directory To Alex with `cd Alex` After Cloning The Repository

5.Run `npm install` to Install The Necessary Dependencies

6.After That Run `npm start` Or `npm run start` To Start Your Api Locally

7.Open Up Your Browser Then Type `localhost:4000` The Api Will Run On 4000 port You Can Change It From `app.js`


8.Then If You Want The List Of The Weapons That Exists In Call Of Duty Mobile type `localhost:4000/cod/guns`



**Contribution Guide**


Please take a look at the contributing  guidelines if
you're interested in helping by any means.
Contribution to this project is not only limited to coding help, You can
suggest a feature, help with docs, design ideas or even some typos. You
are just an issue away.  Don't hesitate to create an issue.
❤️